using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemies : MonoBehaviour
{
    [SerializeField] private int damage;
    [SerializeField] private bool discardAllObjects;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            if (discardAllObjects || damage >= GiveObject.Instance.values.Count)
            {
                GiveObject.Instance.values.Clear();
            }
            else
            {
                for(int i=0; i<damage;i++)
                    GiveObject.Instance.values.Remove(GiveObject.Instance.values[i]);
            }
            Destroy(this);
        }
    }
}
