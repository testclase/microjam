using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GiveObject : Singleton<GiveObject>
{
    [HideInInspector] public List<int> values;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("SafeZone"))
        {
            for(int i=0; i < values.Count; i++)
            {
                Score.Instance.TotalValue += values[i];
                values.Remove(values[i]);
            }
        }
    }
}
