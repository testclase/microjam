﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : Singleton<GameManager>
{
    public void StartGame()
    {
        Timer.Instance.StartTimer();
    }

    public void EndGame()
    {
        // JUEGO TERMINADO
    }
}
